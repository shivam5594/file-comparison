
  #include"file_compare.h"
  /*
  *	@return Difference in size of both files in bytes.
  *
  *	@param1 pointer to a file to compare
  *	@param2 pointer to a file to compare
  *
  *	It takes difference of number of bytes and return it.
  */
long checkIfSizeDiffer(FILE *file1, FILE *file2)
{
 fseek(file1,0,SEEK_END);
 fseek(file2,0,SEEK_END);

 file1Size = ftell(file1);
 file2Size = ftell(file2);

 rewind(file2);
 rewind(file1);

 return file2Size - file1Size;
}

  /*
  *	@return enum bool.
  *		false - if both files are not same or of different size.
  *		true  - if both files are same.
  *
  *	@param1 pointer to a file to compare
  *	@param2 pointer to a file to compare
  *
  * Version - 1
  *	If both files are different then cursor in the file will be set to the char from they differ, which can
  *	be used to print line of difference, otherwise cursor will be at end of files.
  *
  * Version - 2
  * Previous version was just comparing but now printing the difference from this function only to reduce the execution time.
  * Change value of PRINT_FROM_COMPARE to 1 to print from this function.
  */
bool compare_files(FILE *file1, FILE *file2)
{
  startTime = clock();
  int sizeOFarray = 16;
  char line[sizeOFarray];
  char ch1, ch2;
  bool status = false;
  bool Different = false;
  long diff;

  diff_bytes_num = checkIfSizeDiffer(file1, file2);

  if (diff_bytes_num != 0)
  {
    Different = true;
  }

 printf("\n");

 while (((ch1 = fgetc(file1)) != EOF) && ((ch2 = fgetc(file2)) != EOF))
 {
    if (ch1!=ch2)
    {
      fseek(file1, -1, SEEK_CUR);
      fseek(file2, -1, SEEK_CUR);
      status = false;
      break;
    }
    else
    {
  		/*if (count%2)
  		{
  			printf(" ");
  		}
  		printf("%x", ch1);*/
      status = true;
    }
    //count++;
  }

  if (status == true && Different == false)
  {
    stopTime = clock();
    return true;
  }
  else
  {
    #if PRINT_FROM_COMPARE

     if (diff_bytes_num <= 0)
     {
        diff = file1Size - ftell(file1);

        sizeOFarray = (diff > sizeOFarray) ? sizeOFarray : diff;

        fread(line,sizeof(char),sizeOFarray,file1);

        for(int count = 0; count < sizeOFarray; count++)
        {
          printf("%x ", line[count]);
        }
        printf("\n");
      }
      else
      {
        diff = file2Size - ftell(file2);

        sizeOFarray = (diff > sizeOFarray) ? sizeOFarray : diff;

        fread(line,sizeof(char),sizeOFarray,file2);

        for(int count = 0; count < sizeOFarray; count++)
        {
          printf("%x ", line[count]);
        }
        printf("\n");
      }

    #endif 

  stopTime = clock();
  return false;
  }
}


  /*
  *	@return void.
  *
  *	@param1 pointer to a file to compare
  *	@param2 pointer to a file to compare
  *
  *	It prints 16 Bytes from the point both files differ depending on size of both files. Prints bytes from bigger files.
  *	Change value of PRINT_FROM_COMPARE to 0 to print from this function.
  */
void print_difference(FILE *file1, FILE *file2)
{
 int sizeOFarray = 16;
 char line[sizeOFarray];
 long diff;

  	/*
  	*	Here, it is checking that which file is bigger so it can print accordingly.
  	* 	case 1:	Both files are of same size, then it will print from the file which was first parameter.
  	*	
  	*	case 2: First file is bigger than second then it will print from first file to avoid prining garbage from second file.
  	* 	
  	*	case 3: Second file is bigger than first file, then it will print from second.
  	*/

 if (diff_bytes_num <= 0)
 {
    diff = file1Size - ftell(file1);

    sizeOFarray = (diff > sizeOFarray) ? sizeOFarray : diff;

    fread(line,sizeof(char),sizeOFarray,file1);

    for(int count = 0; count < sizeOFarray; count++)
    {
     printf("%x ", line[count]);
    }
    printf("\n");
  }
  else
  {
    diff = file2Size - ftell(file2);

    sizeOFarray = (diff > sizeOFarray) ? sizeOFarray : diff;

    fread(line,sizeof(char),sizeOFarray,file2);

    for(int count = 0; count < sizeOFarray; count++)
    {
     printf("%x ", line[count]);
    }

  printf("\n");
  }
}
