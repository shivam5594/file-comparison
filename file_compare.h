#include<stdio.h>
#include<time.h>

#define SUCCESS 0
#define FAIL -1
#define PRINT_FROM_COMPARE 1

long diff_bytes_num;
int startTime, stopTime;
long file1Size, file2Size;

typedef enum { false, true } bool;

/*
*	@return Difference in size of both files in bytes.
*
*	@param1 pointer to a file to compare
*	@param2 pointer to a file to compare
*
*	It takes difference of number of bytes and return it.
*/
long checkIfSizeDiffer(FILE *file1, FILE *file2);

/*
*	@return enum bool.
*		false - if both files are not same or of different size.
*		true  - if both files are same.
*
*	@param1 pointer to a file to compare
*	@param2 pointer to a file to compare
*
* Version - 1
*	If both files are different then cursor in the file will be set to the char from they differ, which can
*	be used to print line of difference, otherwise cursor will be at end of files.
*
* Version - 2
* Previous version was just comparing but now printing the difference from this function only to reduce the execution time.
* Change value of PRINT_FROM_COMPARE to 1 to print from this function.
*/
bool compare_files(FILE* file1, FILE* file2);

/*
*	@return void.
*
*	@param1 pointer to a file to compare
*	@param2 pointer to a file to compare
*
*	It prints 16 Bytes from the point both files differ depending on size of both files. Prints bytes from bigger files.
*	Change value of PRINT_FROM_COMPARE to 0 to print from this function.
*/
void print_difference(FILE *file1, FILE *file2);
