#include<stdio.h>
#include"file_compare.h"

int main(int argc, char *argv[])
{

	FILE *file1 = NULL, *file2 = NULL;

	printf("\nChecking inputs.");

	if( argc != 3)
	{
		printf("\nERROR: Incorrect inputs. Please enter only 2 files for comparison.");
		return FAIL;
	}
	else
	{
		file1 = fopen(argv[1],"rb");
		if( file1 == NULL )
		{
			printf("\nERROR: Couldn't open file %s", argv[1]);
			return FAIL;
		}

		file2 = fopen(argv[2],"rb");
		if( file2 == NULL )
		{
			printf("\nERROR: Couldn't open file %s", argv[2]);
			return FAIL;
		}

		printf("\nInputs are correct and both files were successfully opened.");

		if (compare_files(file1,file2))
		{
			printf("RESULT: Both files are same.\n");
			printf("Elaspsed time: %f S\n", (float)(stopTime-startTime)/CLOCKS_PER_SEC);
			return SUCCESS;
		}
		else
		{
			// Print 16 bytes from both files differ
			
			#if !PRINT_FROM_COMPARE

			print_difference(file1,file2);

			#endif
			
			printf("RESULT: They differ from byte %ld.\n",ftell(file2)+1);
			printf("Elaspsed time: %f S\n", (float)(stopTime-startTime)/CLOCKS_PER_SEC);
			return FAIL;
		}
	}
}